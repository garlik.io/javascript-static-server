const serverRules = {
  'no-process-env': 'off',
  'no-console': 'off',
};

const launcherRules = {
  'no-global-assign': 'off',
  'no-native-reassign': 'off',
  'import/no-commonjs': 'off',
};

module.exports = {
  "env": {
    "browser": false,
    "es6": true,
    "node": true
  },
  "extends": [
    "canonical",
    "canonical/mocha"
  ],
  'rules': Object.assign(serverRules, launcherRules)
};
