/**
 * Copy to your projects root as .serverconfigrc.js
 *
 * @type {{port: number, host: string}}
 */
module.exports = {
  host: '127.0.0.1',
  port: 8080
};
