import path from 'path';
import express from 'express';
import morgan from 'morgan';
import config from './config';

const {serverRoot: publicRoot} = config;
const app = express();

/**
 * Log all requests
 */
app.use(morgan('tiny'));

/**
 * Set content directories
 */
app.use(express.static(publicRoot));

app.get('/', (request, response) => {
  response.sendfile(path.join(publicRoot, 'index.html'));
});

app.listen(config.port, () => {
  console.log(`Listening on http://${config.host}:${config.port}`);
});
